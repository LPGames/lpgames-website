(function($) {
    $(document).ready(function () {
        const btn = document.querySelector('.poptrox-popup');
        const options = {
            attributes: true
        };

        function callback(mutationList, observer) {
            mutationList.forEach(function(mutation) {
                if (mutation.type === 'attributes' && mutation.attributeName === 'class') {
                    const classes = mutation.target.className;
                    console.log(`The classes changed to ${classes}`);

                    if (classes.substring(classes.length - 7) === "loading") {
                        hideText();
                    }
                    else {
                        addText();
                    }
                }
            })
        }

        function hideText() {
            // Now create and append to iDiv
            $('.caption').unbind("click");
        }

        function addText() {
            // Now create and append to iDiv
            const $caption = $('.poptrox-popup .caption');
            console.log($caption);
            const link = $caption.html();
            $caption.html("Click here")
            console.log(link);
            if (link.length > 0) {
                $caption.click(function() {
                    window.open(link, '_blank').focus();
                });
            }

        }

        const observer = new MutationObserver(callback);
        observer.observe(btn, options);
        console.log(btn);
    });
})(jQuery);
console.log('test');
